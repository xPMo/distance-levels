#!/bin/sh
IFS='
	'
dest=$HOME/.config/refract/Distance
if [ -d $dest ]; then
	echo >&2 "$dest already exists, is it a link to here?"
else
	ln -s "$(realpath $(dirname $0))/LEVELS" "$dest/Levels/MyLevels"
	ln -s "$(realpath $(dirname $0))/OBJECTS" "$dest/CustomObjects"
fi
