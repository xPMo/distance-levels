#!/bin/sh
tab='	'
IFS='
	'
inotifywait --event ATTRIB --format "%w$tab%f" --monitor -r * |
while read -r dir file; do
	git add "$dir/$file" &&
	git commit -m "Update ${file%.*}" &&
	git push
done

